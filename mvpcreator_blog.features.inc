<?php
/**
 * @file
 * mvpcreator_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mvpcreator_blog_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function mvpcreator_blog_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function mvpcreator_blog_node_info() {
  $items = array(
    'mvpcreator_blog' => array(
      'name' => t('Blog post'),
      'base' => 'node_content',
      'description' => t('An article in the blog.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
